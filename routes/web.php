<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::auth();

Route::group(['middleware'=>'auth'], function() {
	Route::get('tipe', 'TipeController@index');
	Route::get('tipe/add', 'TipeController@create');
	Route::post('tipe/add', 'TipeController@store');
	Route::get('tipe/{id}/edit', 'TipeController@edit');
	Route::patch('tipe/{id}/edit', 'TipeController@update');
	Route::delete('tipe/{id}/delete', 'TipeController@destroy');

	Route::get('merek', 'MerekController@index');
	Route::get('merek/add', 'MerekController@create');
	Route::post('merek/add', 'MerekController@store');
	Route::get('merek/{id}/edit', 'MerekController@edit');
	Route::patch('merek/{id}/edit', 'MerekController@update');
	Route::delete('merek/{id}/delete', 'MerekController@destroy');
	
	Route::get('/', 'SilinderController@index');
	Route::get('silinder/add', 'SilinderController@create');
	Route::post('silinder/add', 'SilinderController@store');
	Route::get('silinder/{id}/edit', 'SilinderController@edit');
	Route::patch('silinder/{id}/edit', 'SilinderController@update');
	Route::delete('silinder/{id}/delete', 'SilinderController@destroy');

	Route::get('motor', 'MotorController@index');
	Route::get('motor/add', 'MotorController@create');
	Route::post('motor/add', 'MotorController@store');
	Route::get('motor/{id}/edit', 'MotorController@edit');
	Route::patch('motor/{id}/edit', 'MotorController@update');
	Route::delete('motor/{id}/delete', 'MotorController@destroy');
});

	Route::get('costumer', 'CostumerController@index');
	Route::get('costumer/add', 'CostumerController@create');
	Route::post('costumer/add', 'CostumerController@store');
	Route::get('costumer/{id}/edit', 'CostumerController@edit');
	Route::patch('costumer/{id}/edit', 'CostumerController@update');
	Route::delete('costumer/{id}/delete', 'CostumerController@destroy');