<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Merek extends Model
{
    //
    use SoftDeletes;

    protected $table		= 'merek';
    protected $primaryKey	= 'kode_merek';

    protected $fillable = [
    	'nama_merek'
    ];
}
