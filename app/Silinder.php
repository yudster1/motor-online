<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Silinder extends Model
{
    // Digunakan untuk menggunakan soft delete secara default saat menghapus data
	use SoftDeletes;

    protected $table 		= 'silinder';
    protected $primaryKey 	= 'kode_silinder';

    protected $fillable = [
    	'ukuran_silinder'
    ];
}
