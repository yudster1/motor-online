<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MotorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['result'] = \App\Motor::all();
        return view('Motor/index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('motor/form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nama_motor'		=>	'required|max:100',
            'kode_tipe'			=>	'required|exists:tipe',
            'kode_silinder'		=>	'required|exists:silinder',
            'kode_merek'		=>	'required|exists:merek',
            'kapasitas_bensin'	=>	'required',
            'harga'				=>	'required',
        ];
        $this->validate($request, $rules);

        $input = $request->all();
        $status = \App\Motor::create($input);

        if ($status) return redirect('motor')->with('success', 'Data berhasil ditambahkan');
        else return redirect('motor')->with('error', 'Terjadi kesalahan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
    public function show($id)
    {
        $tipe = Tipe::findOrFail($id);
        return view('tipe.show', compact('tipe'));
    }
    */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['result'] = \App\Motor::where('kode_motor', $id)->first();
        return view('motor/form')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'nama_motor'		=>	'required|max:100',
            'kode_tipe'			=>	'required|exists:tipe',
            'kode_silinder'		=>	'required|exists:silinder',
            'kode_merek'		=>	'required|exists:merek',
            'kapasitas_bensin'	=>	'required',
            'harga'				=>	'required',
        ];
        $this->validate($request, $rules);

        $input  =   $request->all();
        $result =   \App\Motor::where('kode_motor', $id)->first();
        $status =   $result->update($input);

        if ($status) return redirect('motor')->with('success', 'Data berhasil diubah');
        else return redirect('motor')->with('error', 'Data gagal diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $result = \App\Motor::where('kode_motor', $id)->first();
        $status = $result->delete();

        if($status) return redirect('/')->with('success', 'Data berhasil dihapus');
        else return redirect('motor')->with('error', 'Data gagal dihapus');
    }
}
