<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['result'] = \App\Tipe::all();
        return view('tipe/index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipe/form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'tipe_motor'    =>  'required|max:100'
        ];
        $this->validate($request, $rules);

        $input = $request->all();
        $status = \App\Tipe::create($input);

        if ($status) return redirect('tipe')->with('success', 'Data berhasil ditambahkan');
        else return redirect('tipe')->with('error', 'Terjadi kesalahan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
    public function show($id)
    {
        $tipe = Tipe::findOrFail($id);
        return view('tipe.show', compact('tipe'));
    }
    */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['result'] = \App\Tipe::where('kode_tipe', $id)->first();
        return view('tipe/form')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'tipe_motor'    =>  'required|max:100'
        ];
        $this->validate($request, $rules);

        $input  =   $request->all();
        $result =   \App\Tipe::where('kode_tipe', $id)->first();
        $status =   $result->update($input);

        if ($status) return redirect('tipe')->with('success', 'Data berhasil diubah');
        else return redirect('tipe')->with('error', 'Data gagal diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $result = \App\Tipe::where('kode_tipe', $id)->first();
        $status = $result->delete();

        if($status) return redirect('/')->with('success', 'Data berhasil dihapus');
        else return redirect('/')->with('error', 'Data gagal dihapus');
    }
}
