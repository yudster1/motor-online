<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CostumerController extends Controller
{
     public function index()
    {
        $data['result'] = \App\Costumer::all();
        return view('Costumer/index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('costumer/form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nama_lengkap'			=>	'required|max:100',
            'alamat'				=>	'required',
            'no_telp'				=>	'required',
            'alamat_email'			=>	'required',
            'password'				=>	'required',
        ];
        $this->validate($request, $rules);

        $input = $request->all();
        $status = \App\Costumer::create($input);

        if ($status) return redirect('costumer')->with('success', 'Data berhasil ditambahkan');
        else return redirect('costumer')->with('error', 'Terjadi kesalahan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
    public function show($id)
    {
        $tipe = Tipe::findOrFail($id);
        return view('tipe.show', compact('tipe'));
    }
    */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['result'] = \App\Costumer::where('id_costumer', $id)->first();
        return view('costumer/form')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'nama_lengkap'			=>	'required|max:100',
            'alamat'				=>	'required',
            'no_telp'				=>	'required',
            'alamat_email'			=>	'required',
            'password'				=>	'required',
        ];
        $this->validate($request, $rules);

        $input  =   $request->all();
        $result =   \App\Costumer::where('id_costumer', $id)->first();
        $status =   $result->update($input);

        if ($status) return redirect('costumer')->with('success', 'Data berhasil diubah');
        else return redirect('costumer')->with('error', 'Data gagal diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $result = \App\Costumer::where('id_costumer', $id)->first();
        $status = $result->delete();

        if($status) return redirect('costumer')->with('success', 'Data berhasil dihapus');
        else return redirect('costumer')->with('error', 'Data gagal dihapus');
    }
}
