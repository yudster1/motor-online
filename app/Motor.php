<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Motor extends Model
{
    public $primaryKey = 'kode_motor';

    protected $table = 'motor';

    protected $fillable = ['kode_motor', 'nama_motor', 'kode_tipe', 'kode_silinder', 'kode_merek', 'kapasitas_bensin', 'harga'];

    public function tipe()
    {
    	return $this->hasOne('App\Tipe', 'kode_tipe', 'kode_tipe');
    }

    public function silinder()
    {
    	return $this->hasOne('App\Silinder', 'kode_silinder', 'kode_silinder');
    }

    public function merek()
    {
    	return $this->hasOne('App\Merek', 'kode_merek', 'kode_merek');
    }
}
