<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Costumer extends Model
{
    public $primaryKey = 'id_costumer';

    protected $table = 'costumer';

    protected $fillable = ['id_costumer', 'nama_lengkap', 'alamat', 'no_telp', 'alamat_email', 'password'];
}
