<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tipe extends Model
{
	// Digunakan untuk menggunakan soft delete secara default saat menghapus data
	use SoftDeletes;

    protected $table 		= 'tipe';
    public $primaryKey 	= 'kode_tipe';

    protected $fillable = [
    	'tipe_motor'
    ];
}
