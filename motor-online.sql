-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 15, 2018 at 06:31 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `motor-online`
--

-- --------------------------------------------------------

--
-- Table structure for table `costumer`
--

CREATE TABLE `costumer` (
  `id_costumer` int(10) UNSIGNED NOT NULL,
  `nama_lengkap` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `costumer`
--

INSERT INTO `costumer` (`id_costumer`, `nama_lengkap`, `alamat`, `no_telp`, `alamat_email`, `password`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Yudhaputera Wardiman', 'Jln. Marga Kencana Tengah No 39A', '087822166197', 'resiloreid@gmail.com', '$2y$10$G1jukMgiRNhrYR78DCd8MuKBxXJHsG/QUeRjDnmGnwrEKnEMSOiSO', '2018-07-15 05:33:39', '2018-07-15 05:33:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `merek`
--

CREATE TABLE `merek` (
  `kode_merek` int(10) UNSIGNED NOT NULL,
  `nama_merek` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `merek`
--

INSERT INTO `merek` (`kode_merek`, `nama_merek`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Yamaha', '2018-07-14 07:05:05', '2018-07-14 07:05:05', NULL),
(2, 'Honda', '2018-07-14 07:05:11', '2018-07-14 07:05:11', NULL),
(3, 'Kawasaki', '2018-07-14 07:05:17', '2018-07-14 07:05:17', NULL),
(4, 'Suzuki', '2018-07-14 07:05:22', '2018-07-14 07:05:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_05_12_021343_create_costumer_table', 1),
(4, '2018_05_12_021421_create_tipe_table', 1),
(5, '2018_05_12_021453_create_silinder_table', 1),
(6, '2018_05_12_021519_create_merek_table', 1),
(7, '2018_05_12_022204_create_motor_table', 1),
(8, '2018_05_12_022221_create_pemesanan_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `motor`
--

CREATE TABLE `motor` (
  `kode_motor` int(10) UNSIGNED NOT NULL,
  `nama_motor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_tipe` int(11) NOT NULL,
  `kode_silinder` int(11) NOT NULL,
  `kode_merek` int(11) NOT NULL,
  `kapasitas_bensin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `motor`
--

INSERT INTO `motor` (`kode_motor`, `nama_motor`, `kode_tipe`, `kode_silinder`, `kode_merek`, `kapasitas_bensin`, `harga`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Revo X', 1, 3, 2, '4 Liter', '15.550.000', '2018-07-14 07:13:36', '2018-07-14 07:13:36', NULL),
(2, 'Blade 125 FI Disc Brake', 1, 5, 2, '4 Liter', '16.675.000', '2018-07-14 07:54:13', '2018-07-14 07:54:13', NULL),
(3, 'All New Vixion', 3, 1, 1, '11 Liter', '26.250.000', '2018-07-14 07:58:50', '2018-07-14 07:58:50', NULL),
(4, 'New Smash FI R', 1, 3, 4, '3,7 Liter', '13.250.000', '2018-07-14 08:16:31', '2018-07-14 08:16:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pemesanan`
--

CREATE TABLE `pemesanan` (
  `kode_pesan` int(10) UNSIGNED NOT NULL,
  `id_costumer` int(11) NOT NULL,
  `kode_motor` int(11) NOT NULL,
  `kode_tipe` int(11) NOT NULL,
  `kode_silinder` int(11) NOT NULL,
  `kode_merek` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL,
  `pesan_opsional` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Aktif','Batal') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `silinder`
--

CREATE TABLE `silinder` (
  `kode_silinder` int(10) UNSIGNED NOT NULL,
  `ukuran_silinder` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `silinder`
--

INSERT INTO `silinder` (`kode_silinder`, `ukuran_silinder`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '150cc', '2018-07-14 07:08:37', '2018-07-14 07:08:37', NULL),
(2, '250cc', '2018-07-14 07:08:41', '2018-07-14 07:08:41', NULL),
(3, '110cc', '2018-07-14 07:08:45', '2018-07-14 07:08:45', NULL),
(4, '250cc', '2018-07-14 07:08:48', '2018-07-14 07:53:32', '2018-07-14 07:53:32'),
(5, '125cc', '2018-07-14 07:53:26', '2018-07-14 07:53:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tipe`
--

CREATE TABLE `tipe` (
  `kode_tipe` int(10) UNSIGNED NOT NULL,
  `tipe_motor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tipe`
--

INSERT INTO `tipe` (`kode_tipe`, `tipe_motor`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Bebek', '2018-07-14 07:08:57', '2018-07-14 07:08:57', NULL),
(2, 'Matic', '2018-07-14 07:09:03', '2018-07-14 07:09:03', NULL),
(3, 'Sport', '2018-07-14 07:09:09', '2018-07-14 07:09:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '9e.yudhaputera.w@gmail.com', '$2y$10$41asux29Ti8vMHBbCJUq8.u1cyuQHEzKOcPD86TrmQlz7m0Cinqy.', 'EpUeC2kP1S5Y6SmR3zJak5irv8OGx2PxqeQulWtluEhit2NDPAX8hOri69wO', '2018-07-14 09:58:29', '2018-07-14 09:58:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `costumer`
--
ALTER TABLE `costumer`
  ADD PRIMARY KEY (`id_costumer`);

--
-- Indexes for table `merek`
--
ALTER TABLE `merek`
  ADD PRIMARY KEY (`kode_merek`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `motor`
--
ALTER TABLE `motor`
  ADD PRIMARY KEY (`kode_motor`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD PRIMARY KEY (`kode_pesan`);

--
-- Indexes for table `silinder`
--
ALTER TABLE `silinder`
  ADD PRIMARY KEY (`kode_silinder`);

--
-- Indexes for table `tipe`
--
ALTER TABLE `tipe`
  ADD PRIMARY KEY (`kode_tipe`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `costumer`
--
ALTER TABLE `costumer`
  MODIFY `id_costumer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `merek`
--
ALTER TABLE `merek`
  MODIFY `kode_merek` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `motor`
--
ALTER TABLE `motor`
  MODIFY `kode_motor` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pemesanan`
--
ALTER TABLE `pemesanan`
  MODIFY `kode_pesan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `silinder`
--
ALTER TABLE `silinder`
  MODIFY `kode_silinder` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tipe`
--
ALTER TABLE `tipe`
  MODIFY `kode_tipe` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
