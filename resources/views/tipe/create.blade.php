@extends('layouts.app')

@section('content')
<h4>Tipe Baru</h4>
<form action="{{ route('tipe.store') }}" method="post">
    {{csrf_field()}}
    <div class="form-group {{ $errors->has('tipe_motor') ? 'has-error' : '' }}">
        <label for="tipe_motor" class="control-label">Tipe</label>
        <input type="text" class="form-control" name="tipe_motor" placeholder="Tipe Motor">
        @if ($errors->has('tipe_motor'))
            <span class="help-block">{{ $errors->first('tipe_motor') }}</span>
        @endif
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Simpan</button>
        <a href="{{ route('tipe.index') }}" class="btn btn-default">Kembali</a>
    </div>
</form>
@endsection