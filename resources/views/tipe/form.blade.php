@extends('templates/header')

@section('content')

	<section class="content-header">
		<h1>
			{{ empty($result) ? 'Tambah' : 'Edit' }} Data Tipe
			<small>Data Tipe Motor</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('tipe') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Data Tipe Motor</li>
			<li class="active">{{ empty($result) ? 'Tambah' : 'Edit' }} Data Tipe</li>
		</ol>
	</section>

	<section class="content">
		@include('templates/feedback')
		<div class="box">
			<div class="box-header with-border">
				<a href="{{ url('/') }}" class="btn bg-purple"><i class="fa fa-chevron-left"></i> Kembali</a>
			</div>
			<div class="box-body">
				<form action="{{ empty($result) ? url('tipe/add') : url("tipe/$result->kode_tipe/edit") }}" class="form-horizontal" method="POST">
					{{ csrf_field() }}

					@if (!empty($result))
						{{ method_field('patch') }}
					@endif
					<div class="form-group">
						<label class="control-label col-sm-2">Nama Tipe</label>
						<div class="col-sm-10">
							<input type="text" name="tipe_motor" class="form-control" placeholder="Masukan Nama Tipe" value="{{ @$result->tipe_motor }}" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-10 col-sm-offset-2">
							<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>

@endsection