@extends('layouts.app')

@section('content')
<h4>Ubah Silinder</h4>
<form action="{{ route('silinder.update', $silinder->kode_silinder) }}" method="post">
    {{csrf_field()}}
    {{ method_field('PUT') }}
    <div class="form-group {{ $errors->has('ukuran_silinder') ? 'has-error' : '' }}">
        <label for="ukuran_silinder" class="control-label">Silinder</label>
        <input type="text" class="form-control" name="ukuran_silinder" placeholder="Silinder Motor" value="{{ $silinder->ukuran_silinder }}">
        @if ($errors->has('ukuran_silinder'))
            <span class="help-block">{{ $errors->first('ukuran_silinder') }}</span>
        @endif
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Simpan</button>
        <a href="{{ route('silinder.index') }}" class="btn btn-default">Kembali</a>
    </div>
</form>
@endsection