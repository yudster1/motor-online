@extends('layouts.app')

@section('content')
<h4>Silinder Baru</h4>
<form action="{{ route('silinder.store') }}" method="post">
    {{csrf_field()}}
    <div class="form-group {{ $errors->has('ukuran_silinder') ? 'has-error' : '' }}">
        <label for="ukuran_silinder" class="control-label">Silinder</label>
        <input type="text" class="form-control" name="ukuran_silinder" placeholder="Silinder Motor">
        @if ($errors->has('ukuran_silinder'))
            <span class="help-block">{{ $errors->first('ukuran_silinder') }}</span>
        @endif
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Simpan</button>
        <a href="{{ route('silinder.index') }}" class="btn btn-default">Kembali</a>
    </div>
</form>
@endsection