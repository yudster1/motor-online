@extends('templates/header')

@section('content')

	<section class="content-header">
		<h1>
			{{ empty($result) ? 'Tambah' : 'Edit' }} Data Merek
			<small>Data Merek Motor</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('merek') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Data Merek Motor</li>
			<li class="active">{{ empty($result) ? 'Tambah' : 'Edit' }} Data Merek</li>
		</ol>
	</section>

	<section class="content">
		@include('templates/feedback')
		<div class="box">
			<div class="box-header with-border">
				<a href="{{ url('/') }}" class="btn bg-purple"><i class="fa fa-chevron-left"></i> Kembali</a>
			</div>
			<div class="box-body">
				<form action="{{ empty($result) ? url('merek/add') : url("merek/$result->kode_merek/edit") }}" class="form-horizontal" method="POST">
					{{ csrf_field() }}

					@if (!empty($result))
						{{ method_field('patch') }}
					@endif
					<div class="form-group">
						<label class="control-label col-sm-2">Nama Merek</label>
						<div class="col-sm-10">
							<input type="text" name="nama_merek" class="form-control" placeholder="Masukan Nama Merek" value="{{ @$result->nama_merek }}" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-10 col-sm-offset-2">
							<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>

@endsection