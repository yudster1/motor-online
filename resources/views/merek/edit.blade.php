@extends('layouts.app')

@section('content')
<h4>Ubah Merek</h4>
<form action="{{ route('merek.update', $merek->kode_merek) }}" method="post">
    {{csrf_field()}}
    {{ method_field('PUT') }}
    <div class="form-group {{ $errors->has('nama_merek') ? 'has-error' : '' }}">
        <label for="nama_merek" class="control-label">Merek</label>
        <input type="text" class="form-control" name="nama_merek" placeholder="Tipe Motor" value="{{ $merek->nama_merek }}">
        @if ($errors->has('nama_merek'))
            <span class="help-block">{{ $errors->first('nama_merek') }}</span>
        @endif
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Simpan</button>
        <a href="{{ route('merek.index') }}" class="btn btn-default">Kembali</a>
    </div>
</form>
@endsection