@extends('templates/header')

@section('content')

	<section class="content-header">
		<h1>
			{{ empty($result) ? 'Tambah' : 'Edit' }} Data Costumer
			<small>Data Costumer</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Data Costumer</li>
			<li class="active">{{ empty($result) ? 'Tambah' : 'Edit' }} Data Costumer</li>
		</ol>
	</section>

	<section class="content">
		@include('templates/feedback')
		<div class="box">
			<div class="box-header with-border">
				<a href="{{ url('costumer') }}" class="btn bg-purple"><i class="fa fa-chevron-left"></i> Kembali</a>
			</div>
			<div class="box-body">
				<form action="{{ empty($result) ? url('costumer/add') : url("costumer/$result->id_costumer/edit") }}" class="form-horizontal" method="POST">
					{{ csrf_field() }}

					@if (!empty($result))
						{{ method_field('patch') }}
					@endif
					<div class="form-group">
						<label class="control-label col-sm-2">Nama Lengkap</label>
						<div class="col-sm-10">
							<input type="text" name="nama_lengkap" class="form-control" placeholder="Masukan Nama Lengkap" value="{{ @$result->nama_lengkap }}" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2">Alamat</label>
						<div class="col-sm-10">
							<input type="text" name="alamat" class="form-control" placeholder="Masukan Alamat" value="{{ @$result->alamat }}" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2">No Telp</label>
						<div class="col-sm-10">
							<input type="text" name="no_telp" class="form-control" placeholder="Masukan No Telp" value="{{ @$result->no_telp }}" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2">Alamat Email</label>
						<div class="col-sm-10">
							<input type="text" name="alamat_email" class="form-control" placeholder="Masukan Alamat Email" value="{{ @$result->alamat_email }}" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2">Password</label>
						<div class="col-sm-10">
							<input type="password" name="password" class="form-control" placeholder="Masukan Password" value="{{ @$result->password }}" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-10 col-sm-offset-2">
							<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>

@endsection