@extends('templates/header')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Motor
        <small>Motor</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Motor</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    @include('templates/feedback')
      <div class="box">
        <div class="box-header with-border">
          <a href="{{ url('motor/add') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Tambah </a>
        </div>
        <div class="box-body">
          <table class="table table-stripped">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Motor</th>
                <th>Tipe</th>
                <th>Silinder</th>
                <th>Merek</th>
                <th>Kapasitas Bensin</th>
                <th>Harga</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              @foreach ($result as $row)
              <tr>
                <td>{{ !empty($i) ? ++$i: $i = 1 }}</td>
                <td>{{ @$row->nama_motor }}</td>
                <td>{{ @$row->tipe->tipe_motor }}</td>
                <td>{{ @$row->silinder->ukuran_silinder }}</td>
                <td>{{ @$row->merek->nama_merek }}</td>
                <td>{{ @$row->kapasitas_bensin }}</td>
                <td>{{ @$row->harga }}</td>
                <td>
                  <a href="{{ url("motor/$row->kode_motor/edit") }}" class="btn btn-sm 
                  btn-warning"><i class="fa fa-pencil"></i></a>
                  <form action="{{ url("motor/$row->kode_motor/delete") }}"
                  method="POST" style="display:inline;">
                      {{ csrf_field() }}
                      {{ method_field('DELETE') }}
                      <button class="btn btn-sm btn-danger"><i class="fa fa-trash"
                      ></i></button></a>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>

    </section>
    <!-- /.content -->
    @endsection