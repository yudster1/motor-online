@extends('templates/header')

@section('content')

	<section class="content-header">
		<h1>
			{{ empty($result) ? 'Tambah' : 'Edit' }} Data Motor
			<small>Data Motor</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('motor') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Data Motor</li>
			<li class="active">{{ empty($result) ? 'Tambah' : 'Edit' }} Data Motor</li>
		</ol>
	</section>

	<section class="content">
		@include('templates/feedback')
		<div class="box">
			<div class="box-header with-border">
				<a href="{{ url('/') }}" class="btn bg-purple"><i class="fa fa-chevron-left"></i> Kembali</a>
			</div>
			<div class="box-body">
				<form action="{{ empty($result) ? url('motor/add') : url("motor/$result->kode_motor/edit") }}" class="form-horizontal" method="POST">
					{{ csrf_field() }}

					@if (!empty($result))
						{{ method_field('patch') }}
					@endif
					<div class="form-group">
						<label class="control-label col-sm-2">Nama Motor</label>
						<div class="col-sm-10">
							<input type="text" name="nama_motor" class="form-control" placeholder="Masukan Nama Motor" value="{{ @$result->ukuran_motor }}" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2">Tipe</label>
						<div class="col-sm-10">
							<select name="kode_tipe" class="form-control">
								@foreach (\App\Tipe::all() as $tipe)
									<option value="{{ $tipe->kode_tipe }}" {{ @$result->kode_tipe ==
									$tipe->kode_tipe ? 'selected' : '' }}>{{ $tipe->tipe_motor }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2">Silinder</label>
						<div class="col-sm-10">
							<select name="kode_silinder" class="form-control">
								@foreach (\App\Silinder::all() as $silinder)
									<option value="{{ $silinder->kode_silinder }}" {{ @$result->kode_silinder ==
									$silinder->kode_silinder ? 'selected' : '' }}>{{ $silinder->ukuran_silinder }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2">Merek</label>
						<div class="col-sm-10">
							<select name="kode_merek" class="form-control">
								@foreach (\App\Merek::all() as $merek)
									<option value="{{ $merek->kode_merek }}" {{ @$result->kode_merek ==
									$merek->kode_merek ? 'selected' : '' }}>{{ $merek->nama_merek }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2">Kapasitas Bensin</label>
						<div class="col-sm-10">
							<input type="text" name="kapasitas_bensin" class="form-control" placeholder="Masukan Kapasitas" value="{{ @$result->kapasitas_bensin }}" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2">Harga</label>
						<div class="col-sm-10">
							<input type="text" name="harga" class="form-control" placeholder="Masukan Harga" value="{{ @$result->harga }}" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-10 col-sm-offset-2">
							<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>

@endsection