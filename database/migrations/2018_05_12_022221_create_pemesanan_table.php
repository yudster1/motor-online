<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemesananTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemesanan', function (Blueprint $table) {
            $table->increments('kode_pesan');
            $table->integer('id_costumer');
            $table->integer('kode_motor');
            $table->integer('kode_tipe');
            $table->integer('kode_silinder');
            $table->integer('kode_merek');
            $table->integer('jumlah');
            $table->integer('total_harga');
            $table->string('pesan_opsional');
            $table->enum('status', ['Aktif', 'Batal']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemesanan');
    }
}
